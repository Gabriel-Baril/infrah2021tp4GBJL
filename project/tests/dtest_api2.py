import unittest
import requests
import json
from myAPI import api

IP = "0.0.0.0"
PORT = "5555"
URL = "http://" + IP + ":" + PORT

class APITest2(unittest.TestCase):

	@classmethod
	def setUpClass(cls):
		with open("./project/files/MemoryLayout3.vsml", "rb") as f:
			myFile = {'file':f}
			response = requests.post(URL + "/upload", files=myFile)
			if response.status_code != 200:
				print(response.content.decode())
				raise Exception("SetUpClass failed for APITest")


	def test_home(self):
		response = requests.get(URL + "/")
		self.assertEqual(200, response.status_code)

	def test_MetaRoute(self):
		response = requests.get(URL + "/metaInfo")
		self.assertEqual(200, response.status_code)
		map = json.loads(response.content.decode('utf-8'))
		map = map['metaInfo']
		self.assertEqual("4D4C4D4C", map['magicNumber'])
		self.assertEqual("56534D4C", map['memoryLayoutType'])

		self.assertEqual(8, map['pageSize'])
		self.assertEqual(128, map['nbPhysicalPages'])
		self.assertEqual(0, map['nbSwapPages'])
		self.assertEqual(1, map['nbBitmapPages'])
		self.assertEqual(8, map['nbPTPages'])
		self.assertEqual(1, map['nbPLTPages'])

	def test_BitmapRoute(self):
		response = requests.get(URL + "/bitmapInfo")
		self.assertEqual(200, response.status_code)
		map = json.loads(response.content.decode('utf-8'))
		self.assertEqual([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 28, 42, 84, 100, 101], map['bitmapInfo'])

	def test_ptRoute(self):
		response = requests.get(URL + "/ptInfo")
		self.assertEqual(200, response.status_code)
		map = json.loads(response.content.decode('utf-8'))
		map = map['ptInfo']
		self.assertEqual({"ACL": [0], "isSwappedOut": False, "pageID": 0, "pageLocation": 0}, map["0"])
		self.assertEqual({"ACL": [0], "isSwappedOut": False, "pageID": 1, "pageLocation": 1}, map["1"])
		self.assertEqual({"ACL": [0, 5], "isSwappedOut": False, "pageID": 101, "pageLocation": 101}, map["101"])

	def test_pltRoute(self):
		response = requests.get(URL + "/pltInfo")
		self.assertEqual(200, response.status_code)
		map = json.loads(response.content.decode('utf-8'))
		map = map['pltInfo']
		self.assertEqual({'0': [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]}, map[0])
		self.assertEqual({'2': [28]}, map[1])
		self.assertEqual({'3': [84, 42]}, map[2])
		self.assertEqual({'4': [100]}, map[3])
		self.assertEqual({'5': [101]}, map[4])


if __name__ == "__main__":
	unittest.main()
