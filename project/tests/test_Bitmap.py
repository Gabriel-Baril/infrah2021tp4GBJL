import unittest

from myCode import BitmapPage

class BitmapPageTests(unittest.TestCase):

	def testgetBitmapPageInfoWithFile1(self):
		bitmapInfo = BitmapPage.getAvailablePages('FA10860080000000000000000000000000000000000000000000000000000000', 32)
		self.assertEqual([0, 1, 2, 3, 4, 6, 11, 16, 21, 22, 32], bitmapInfo)

	def testgetBitmapPageInfoWithFile3(self):
		bitmapInfo = BitmapPage.getAvailablePages('FFE0000800200000000008000C00000000000000000000000000000000000000', 32)
		self.assertEqual([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 28, 42, 84, 100, 101], bitmapInfo)
