import unittest
import requests
import json
from myAPI import api

IP = "0.0.0.0"
PORT = "5555"
URL = "http://" + IP + ":" + PORT

class APITest(unittest.TestCase):

	@classmethod
	def setUpClass(cls):
		with open("./project/files/MemoryLayout1.vsml", "rb") as f:
			myFile = {'file':f}
			response = requests.post(URL + "/upload", files=myFile)
			if response.status_code != 200:
				print(response.content.decode())
				raise Exception("SetUpClass failed for APITest")


	def test_home(self):
		response = requests.get(URL + "/")
		self.assertEqual(200, response.status_code)

	def test_MetaRoute(self):
		response = requests.get(URL + "/metaInfo")
		self.assertEqual(200, response.status_code)
		map = json.loads(response.content.decode('utf-8'))
		map = map['metaInfo']
		self.assertEqual("4D4C4D4C", map['magicNumber'])
		self.assertEqual("56534D4C", map['memoryLayoutType'])

		self.assertEqual(16, map['pageSize'])
		self.assertEqual(64, map['nbPhysicalPages'])
		self.assertEqual(0, map['nbSwapPages'])
		self.assertEqual(1, map['nbBitmapPages'])
		self.assertEqual(2, map['nbPTPages'])
		self.assertEqual(1, map['nbPLTPages'])

	def test_BitmapRoute(self):
		response = requests.get(URL + "/bitmapInfo")
		self.assertEqual(200, response.status_code)
		map = json.loads(response.content.decode('utf-8'))
		self.assertEqual([0, 1, 2, 3, 4, 6, 11, 16, 21, 22, 32], map['bitmapInfo'])
		
	def test_ptRoute(self):
		response = requests.get(URL + "/ptInfo")
		self.assertEqual(200, response.status_code)
		map = json.loads(response.content.decode('utf-8'))
		map = map['ptInfo']
		self.assertEqual({"ACL": [0], "isSwappedOut": False, "pageID": 0, "pageLocation": 0}, map["0"])
		self.assertEqual({"ACL": [0], "isSwappedOut": False, "pageID": 1, "pageLocation": 1}, map["1"])
		self.assertEqual({"ACL": [0, 44, 8], "isSwappedOut": False, "pageID": 16, "pageLocation": 16}, map["16"])

	def test_pltRoute(self):
		response = requests.get(URL + "/pltInfo")
		self.assertEqual(200, response.status_code)
		map = json.loads(response.content.decode('utf-8'))
		map = map['pltInfo']
		self.assertEqual({'0': [0, 1, 2, 3, 4]}, map[0])
		self.assertEqual({'8': [16, 32, 21]}, map[1])
		self.assertEqual({'30': [22, 6, 11]}, map[2])
		self.assertEqual({'44': [16]}, map[3])


if __name__ == "__main__":
	unittest.main()
