import unittest

from myCode import PTEntry

class PTEntryTests(unittest.TestCase):

	def test_parsePTEntryWithFile1AndId0(self):
		ptEntryInfo = PTEntry.parsePTEntry('00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000', 0)
		expectedMap = {
			"ACL": [0],
			"isSwappedOut": False,
			"pageID": 0,
			"pageLocation": 0
		}
		self.assertEqual(expectedMap, ptEntryInfo.toMap())

	
	def test_parsePTEntryWithFile1AndId16(self):
		ptEntryInfo = PTEntry.parsePTEntry('00102C08000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000', 16)
		expectedMap = {
			"ACL": [
				0,
				44,
				8
			],
			"isSwappedOut": False,
			"pageID": 16,
			"pageLocation": 16
		}
		self.assertEqual(expectedMap, ptEntryInfo.toMap())