from myCode import MyUtils
from myCode.CustomExceptions import *

# Command: xxd -s 2048 -l 2048 MemoryLayout1.vsml | head 

def getAvailablePages(hexDump, pageSizeInOctet):
    hexBitmap = MyUtils.extractSequence(hexDump, 0, pageSizeInOctet) # TODO: HARDCODED
    bitmap = format(MyUtils.hexToInt(hexBitmap), "040b")
    availablePages = getNonNullBitPosFromBinaryString(bitmap)
    return availablePages

def getNonNullBitPosFromBinaryString(binaryString):
    nonNullBitPos = []
    for i in range(len(binaryString)):
        if(binaryString[i] == '1'):
            nonNullBitPos.append(i)
    return nonNullBitPos
