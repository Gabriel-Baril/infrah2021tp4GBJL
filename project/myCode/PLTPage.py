from myCode import PLTEntry
from myCode import MyUtils

def getPLTPageInfo(hexDump):
	allEntryInfo = []
	nbEntry = int((len(hexDump) / 2) / 32)
	for i in range(nbEntry):
		PLTEntryInfo = PLTEntry.getPLTEntryInfo(hexDump, i)
		if PLTEntryInfo != None:
			allEntryInfo.append(PLTEntryInfo)
	return allEntryInfo
