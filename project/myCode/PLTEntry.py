from myCode import MyUtils

def getPLTEntryInfo(hexDump, idEntry):
        accessedPages = []
        isProcessUsedHex = MyUtils.extractSequence(hexDump, idEntry * 32, 1)
        isProcessUsedBinary = format(MyUtils.hexToInt(isProcessUsedHex), "08b")
        if (isProcessUsedBinary[0] == '0'):
                return None

        for i in range(1, 32):
                hexPage = MyUtils.extractSequence(hexDump, (idEntry * 32) + i, 1)
                page = MyUtils.hexToInt(hexPage)
                if page != 0 or len(accessedPages) == 0:
                        accessedPages.append(page)
        PLTInfoDict = {
                idEntry: accessedPages
        }
        return PLTInfoDict
