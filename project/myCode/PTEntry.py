from myCode import MyUtils
from myCode.CustomExceptions import *

class PTEntry:
	def __init__(self, id, location, isSwappedOut, ACL): # TODO: Add acl
		if isinstance(id, int) and isinstance(location, int) and isinstance(isSwappedOut, bool) and isinstance(ACL, list):
			self.__id = id
			self.__location = location
			self.__isSwappedOut = isSwappedOut
			self.__ACL = ACL
		else:
			raise ValueError("Invalid parameters")

	def getId(self):
		return self.__id
		
	def getLocation(self):
		return self.__location

	def isSwappedOut(self):
		return self.__isSwappedOut
		
	def getACL(self):
		return self.__ACL

	def toMap(self):
		map = dict()
		map['pageID'] = self.__id
		map['pageLocation'] = self.__location
		map['isSwappedOut'] = self.__isSwappedOut
		map['ACL'] = self.__ACL
		return map
	

def getEntry(PTPageDump, entryIndex, pageID):
	entryHexDump = MyUtils.extractSequence(PTPageDump, entryIndex * 64, 64)
	return parsePTEntry(entryHexDump, pageID)

def parsePTEntry(entryHexDump, pageID):
	entryByteData = bytearray.fromhex(entryHexDump)

	isSwappedOut = bool(entryByteData[0]) 
	location = entryByteData[1]
	ACL = [0]
	for i in range(2, len(entryByteData)):
		if entryByteData[i] != 0:
			ACL.append(entryByteData[i])

	return PTEntry(pageID, location, isSwappedOut, ACL)


