from flask import *

from myCode import MetaPage
from myCode import BitmapPage
from myCode import PageBroker
from myCode import PTPage
from myCode import PLTPage
from myCode import PTEntry

app = Flask('memoryanalysis')

@app.route('/')
def welcome(): #pragma: no cover
	name = "GB & JL" #TODO put your name here

	return """
	<form action="/upload" enctype="multipart/form-data" method="POST">
	<p>
		Correction.Welcome to the memory dump analysis API.<br>
		<br> <br>
		This code is provided as a starting point for TP4.
		<br> <br>
		Produced by:""" + name + """
	</p>
	<p>
		Please specify a file to analyze: <br>
		<input type="file" name="file" size="40">
	</p>
	<div>
		<input type="submit" value="Submit">
	</div>
	<p>
		Or analyse the (previously) uploaded file by going to one of the following:</br>
			<ul>
			<li><a href="/metaInfo">metaInfo</a></br></li>
			<li><a href="/bitmapInfo">bitmapInfo</a></br></li>
			<li><a href="/ptInfo">ptInfo</a></br></li>
			<li><a href="/pltInfo">pltInfo</a></br></li>
			</ul>
	</p>
	</form>"""



@app.route('/upload', methods=['POST'])
def upload(): #pragma: no cover
	if 'file' not in request.files:
		return make_response('file missing from upload request.',400)
	file = request.files['file']
	file.save(PageBroker.DUMP_FILE)
	try:
		__getMetaPage()
	except:
		return make_response("The file provided does not appear to be a valid memory dump file.", 400)
	return "file successfully uploaded.</br>Return <a href='/'>home</a>"


def __getMetaPage(): #pragma: no cover
	dumpMetaPage = PageBroker.getPage(0)
	metaPage = MetaPage.parseFromHexDump(dumpMetaPage)
	return metaPage


@app.route('/metaInfo')
def getMetaPageInfo(): #pragma: no cover
	metaPage = __getMetaPage()
	return jsonify({'metaInfo':metaPage.toMap()})


def __getBitmapInfo(): #pragma: no cover
	dumpBitmapPage = PageBroker.getPage(1)
	pageSizeInOctet = __getMetaPage().getNbOctetsInPage()
	availablePages = BitmapPage.getAvailablePages(dumpBitmapPage, pageSizeInOctet)
	return availablePages

@app.route('/bitmapInfo')
def getBitmapPageInfo(): #pragma: no cover
	availablePages = __getBitmapInfo()
	return jsonify({'bitmapInfo':availablePages})

def getNbPtEntries():
	pageSizeInOctet = __getMetaPage().getNbOctetsInPage()
	nbPTEntries = pageSizeInOctet / 64
	return int(nbPTEntries)

def __getPTPageInfo():
	metaPageInfo = __getMetaPage()

	PTPageInfo = {}
	for i in __getBitmapInfo():
		pageIndexOffset = metaPageInfo.getNbMetaPages() + metaPageInfo.getNbBitmapPages()
		currentPageIndexOfEntry = int(i / getNbPtEntries())
		dumpPageOfCurrentEntry = PageBroker.getPage(pageIndexOffset + currentPageIndexOfEntry)
		currentEntry = PTEntry.getEntry(dumpPageOfCurrentEntry, i % getNbPtEntries(), i)
		
		PTPageInfo[i] = currentEntry.toMap()
	return PTPageInfo


def __getPLTPageInfo():
	metaPageInfo = __getMetaPage()
	metaJsonDump = json.dumps(metaPageInfo.toMap())
	metaJsonObject = json.loads(metaJsonDump)

	dumpPLTPage = PageBroker.getPage(metaJsonObject["nbPTPages"] + metaJsonObject["nbBitmapPages"] + 1)
	PLTPageInfo = PLTPage.getPLTPageInfo(dumpPLTPage)

	return PLTPageInfo


@app.route('/ptInfo')
def getPTPageInfo(): #pragma: no cover
	ptInfo = __getPTPageInfo()
	return jsonify({'ptInfo':ptInfo})

@app.route('/pltInfo')
def getPLTPageInfo(): #pragma: no cover
	pltInfo = __getPLTPageInfo()
	return jsonify({'pltInfo':pltInfo})


if __name__ == '__main__': #pragma: no cover
	app.run(debug=True, host='0.0.0.0', port = 5555)
