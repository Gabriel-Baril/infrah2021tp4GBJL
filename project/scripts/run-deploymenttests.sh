echo "Initialising the docker environment..."
./runDocker.sh
echo "Running deployment tests with coverage stats"
coverage run --omit */test_*,*/dtest_*,*__init__.py,/usr/lib/python3/dist-packages/* -m unittest discover -s project -p dtest*.py -v
if [ $? -ne 0 ]; then
	echo "Deployment tests failed"
	exit 1
fi

rm .coverage